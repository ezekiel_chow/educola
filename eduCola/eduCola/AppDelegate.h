//
//  AppDelegate.h
//  eduCola
//
//  Created by Ezekiel Chow on 22/10/2015.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

