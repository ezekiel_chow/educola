//
//  Game.h
//  
//
//  Created by Ezekiel Chow on 09/11/2015.
//
//

#import <UIKit/UIKit.h>
#import "Questions.h"
#import <AYVibrantButton/AYVibrantButton.h>
#import <AVFoundation/AVFoundation.h>
#import "Points.h"

#import "HomeViewController.h"

#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>

@interface Game : UIViewController<UITabBarControllerDelegate,UIAlertViewDelegate>

@property(nonatomic, retain) Questions *questionSet;
@property(nonatomic, retain) Points *pointsData;
@property(nonatomic, retain) UIVisualEffectView *effect1View;
@property(nonatomic, retain) UIVisualEffectView *effect2View;
@property(nonatomic, retain) UIVisualEffectView *effect3View;
@property(nonatomic, retain) UIVisualEffectView *effect4View;
@property(nonatomic, retain) AYVibrantButton *ans1Button;
@property(nonatomic, retain) AYVibrantButton *ans2Button;
@property(nonatomic, retain) AYVibrantButton *ans3Button;
@property(nonatomic, retain) AYVibrantButton *ans4Button;
@property (weak, nonatomic) IBOutlet UIImageView *animalsImageView;
@property AVAudioPlayer *soundPlayer;



@property (weak, nonatomic) IBOutlet UILabel *life;
@property (weak, nonatomic) IBOutlet UILabel *pointlabel;





@end
