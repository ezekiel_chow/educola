//
//  Game.m
//  
//
//  Created by Ezekiel Chow on 09/11/2015.
//
//

#import "Game.h"
#import "Questions.h"
#import <AYVibrantButton/AYVibrantButton.h>


#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import "Points.h"

@interface Game ()

@end

@implementation Game
int adjustProgessLimit =10;
int leveldata=0;
static int gameValue = 0;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //initialise
    self.questionSet = [[Questions alloc]initWithPlist:@"Questions.plist"];
    self.pointsData = [[Points alloc]init];
    self.soundPlayer = [[AVAudioPlayer alloc] init];
    
    //tap gesture for image
    self.animalsImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap)];
    onTap.numberOfTapsRequired = 1;
    onTap.numberOfTouchesRequired = 1; //only need 1 finger
    [self.animalsImageView addGestureRecognizer:onTap];
    
    [self addInButtons];
    
  //  [self gameBarDefault];
    
    //tab bar delegate
    [self.tabBarController setDelegate:self];
    _life.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.life];
    
    _pointlabel.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface update

-(void)addInButtons
{
    //view for buttons
    self.effect1View = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.effect1View.frame = self.view.bounds;
    [self.effect1View setFrame:CGRectMake(self.view.frame.size.width * 0.1f, self.view.frame.size.height * 0.6f, 130.0f, 50.0f)];
    [self.effect1View setBackgroundColor:[UIColor blackColor]];
    [self.effect1View setTag:201];
    [self.view addSubview:self.effect1View];
    
    self.effect2View = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.effect2View.frame = self.view.bounds;
    [self.effect2View setFrame:CGRectMake(self.view.frame.size.width * 0.5f, self.view.frame.size.height * 0.6f, 130.0f, 50.0f)];
    [self.effect2View setBackgroundColor:[UIColor blackColor]];
    [self.effect2View setTag:202];
    [self.view addSubview:self.effect2View];
    
    self.effect3View = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.effect3View.frame = self.view.bounds;
    [self.effect3View setFrame:CGRectMake(self.view.frame.size.width * 0.1f, self.view.frame.size.height * 0.7f, 130.0f, 50.0f)];
    [self.effect3View setBackgroundColor:[UIColor blackColor]];
    [self.effect3View setTag:203];
    [self.view addSubview:self.effect3View];
    
    self.effect4View = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.effect4View.frame = self.view.bounds;
    [self.effect4View setFrame:CGRectMake(self.view.frame.size.width * 0.5f, self.view.frame.size.height * 0.7f, 130.0f, 50.0f)];
    [self.effect4View setBackgroundColor:[UIColor blackColor]];
    [self.effect4View setTag:204];
    [self.view addSubview:self.effect4View];
    
    //buttons in view
    self.ans1Button = [[AYVibrantButton alloc] initWithFrame:CGRectZero style:AYVibrantButtonStyleInvert];
    self.ans1Button.vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.ans1Button.text = @"ans1";
    self.ans1Button.font = [UIFont systemFontOfSize:18.0];
    [self.ans1Button setFrame:CGRectMake(0.0f, 0.0f, 130.0f, 50.0f)];
    [self.ans1Button setTag:1];
    [self.ans1Button addTarget:self action:@selector(checkAnswer:) forControlEvents:UIControlEventTouchUpInside];
    [self.effect1View.contentView addSubview:self.ans1Button];
    
    self.ans2Button = [[AYVibrantButton alloc] initWithFrame:CGRectZero style:AYVibrantButtonStyleInvert];
    self.ans2Button.vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.ans2Button.text = @"ans2";
    self.ans2Button.font = [UIFont systemFontOfSize:18.0];
    [self.ans2Button setFrame:CGRectMake(0.0f, 0.0f, 130.0f, 50.0f)];
    [self.ans2Button setTag:2];
    [self.ans2Button addTarget:self action:@selector(checkAnswer:) forControlEvents:UIControlEventTouchUpInside];
    [self.effect2View.contentView addSubview:self.ans2Button];
    
    self.ans3Button = [[AYVibrantButton alloc] initWithFrame:CGRectZero style:AYVibrantButtonStyleInvert];
    self.ans3Button.vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.ans3Button.text = @"ans3";
    self.ans3Button.font = [UIFont systemFontOfSize:18.0];
    [self.ans3Button setFrame:CGRectMake(0.0f, 0.0f, 130.0f, 50.0f)];
    [self.ans3Button setTag:3];
    [self.ans3Button addTarget:self action:@selector(checkAnswer:) forControlEvents:UIControlEventTouchUpInside];
    [self.effect3View.contentView addSubview:self.ans3Button];
    
    self.ans4Button = [[AYVibrantButton alloc] initWithFrame:CGRectZero style:AYVibrantButtonStyleInvert];
    self.ans4Button.vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.ans4Button.text = @"ans4";
    self.ans4Button.font = [UIFont systemFontOfSize:18.0];
    [self.ans4Button setFrame:CGRectMake(0.0f, 0.0f, 130.0f, 50.0f)];
    [self.ans4Button setTag:4];
    [self.ans4Button addTarget:self action:@selector(checkAnswer:) forControlEvents:UIControlEventTouchUpInside];
    [self.effect4View.contentView addSubview:self.ans4Button];
    
    //initialising array
    self.questionSet.currentQuestion = 0;
    [self.questionSet updateSet];
    [self updateButtons];
    [self updateImage];
}

-(void)updateImage
{
    NSString *imageLocation = [NSString stringWithFormat:@"Animals/%@", self.questionSet.questionImage];
    [self.animalsImageView setImage:[UIImage imageNamed:imageLocation]];
}

-(void)updateButtons
{
    NSLog(@"updateing buttons");
    [self.ans1Button setText:self.questionSet.answer1];
    [self.ans2Button setText:self.questionSet.answer2];
    [self.ans3Button setText:self.questionSet.answer3];
    [self.ans4Button setText:self.questionSet.answer4];
}

#pragma mark - Question methods

- (IBAction)nextQuestion:(id)sender
{
    [self.questionSet nextQuestion];
    [self updateButtons];
    [self updateImage];
    [self resetAll];
}

- (IBAction)previousQuestion:(id)sender
{
    [self.questionSet previousQuestion];
    [self updateButtons];
    [self updateImage];
    [self resetAll];
  //  [self gameBarDefault];
}

-(void)checkAnswer:(id)sender
{
    AYVibrantButton *theButton = (AYVibrantButton *) sender;
    NSString *viewTag = [[NSString alloc] initWithFormat:@"20%ld", (long)theButton.tag];
    UIVisualEffectView *viewSelected = (UIVisualEffectView *)[self.view viewWithTag:[viewTag integerValue]];
    
    //if correct
    if (theButton.tag == [self.questionSet.correctAnswer integerValue])
    {
        viewSelected.backgroundColor = [UIColor greenColor];
        [self blinkOff:theButton];
        [self playSound:@"/sounds/BuzzerCorrect.wav"];
        
        [self.pointsData addOnePoint];
            _life.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.life];
        _pointlabel.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
        }
    else //if wrong
    {
        viewSelected.backgroundColor = [UIColor redColor];
        [self blinkOff:theButton];
        [self playSound:@"/sounds/BuzzerWrong.mp3"];
        
        [self.pointsData removeOnePoint];
            _life.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
        _pointlabel.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
        
    if (self.pointsData.points <0 ) {
            [self.pointsData removeOneLife];
        
            NSLog(@"my life %d", self.pointsData.life);
          _life.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
         _pointlabel.text=[[NSString alloc] initWithFormat:@"%i",self.pointsData.points];
                
                    if(self.pointsData.life==0){
                
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"End Game" message:@"You have no more score" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
            
            [alert setDelegate:self];
            [alert show];
               
                    }
    }
    }
    }


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"close game");
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
}
-(void)resetAll
{
    self.effect1View.backgroundColor = [UIColor blackColor];
    self.effect2View.backgroundColor = [UIColor blackColor];
    self.effect3View.backgroundColor = [UIColor blackColor];
    self.effect4View.backgroundColor = [UIColor blackColor];
    
    if ([self.soundPlayer isPlaying]) {
        [self.soundPlayer stop];
        self.soundPlayer = nil;
    }
}

#pragma mark - Animations

- (void)blinkOff:(UIView *)v
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat animations:^ {
        [UIView setAnimationRepeatCount:2.0f];
        v.alpha = .01;  //don't animate alpha to 0, otherwise you won't be able to interact with it
    } completion:^(BOOL finished) {
        v.alpha = 1.0f;
        
        if (v.tag == [self.questionSet.correctAnswer integerValue]) {
            [self.questionSet nextQuestion];
            [self updateButtons];
            [self updateImage];
            [self resetAll];
        }
        
    }];
}

#pragma mark - Sound & Graphic

-(void)playSound :(NSString *)fileName
{
    NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], fileName];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    NSLog(@"sound location %@", path);
    
    
    self.soundPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:soundUrl error:nil];
    [self.soundPlayer play];
}

-(void)imageTap
{
    NSLog(@"image tapped!");
    [self playSound: [NSString stringWithFormat:@"/sounds/animals/%@.mp3", self.questionSet.sound] ];
}

#pragma mark - Points

//-(void)gameBarDefault{
//    gameValue=0;
//    gameProgressTxt.text=0;
//    lvlLabel.text=@"1";
//}
//
-(void)controlGameValue
{
    static int lvlUp =1;
    gameValue++;
  
    
}

#pragma mark - Tabbar delegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //load NSUserDefaults
    
    NSString *Pdata= [[NSString alloc] initWithFormat:@"%i",self.pointsData.points];  //declar Nsstring to be stored in NSUserDefaults
      NSString *Lifedata= [[NSString alloc] initWithFormat:@"%i",self.pointsData.life];
    
    [prefs setObject:Pdata forKey:@"PointData"];
    [prefs setObject:Lifedata forKey:@"LifeData"];
    
    [prefs synchronize];
    
}
@end
