//
//  HomeViewController.h
//  
//
//  Created by alex kow yong yeng on 08/11/2015.
//
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface HomeViewController : UIViewController


- (IBAction)btnStart:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *homeProgressLabel;
@property (strong, nonatomic) IBOutlet UILabel *homeLvlLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *homeProgressBar;



- (IBAction)btnStart:(id)sender;

@property (strong, nonatomic) IBOutlet UIProgressView *homeProgress;

@property (strong, nonatomic) IBOutlet UILabel *homeProgressTxt;
@property (weak, nonatomic) IBOutlet UILabel *lifedata;
@property (weak, nonatomic) IBOutlet UILabel *pointsdata;

@end
