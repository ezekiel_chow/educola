//
//  Login.h
//  
//
//  Created by Ezekiel Chow on 03/11/2015.
//
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>

@interface Login : UIViewController<PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;






@end
