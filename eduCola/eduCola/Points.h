//
//  Points.h
//  
//
//  Created by Ezekiel Chow on 25/11/2015.
//
//

#import <Foundation/Foundation.h>

@interface Points : NSObject

@property int points;
@property int life;

-(void)addOnePoint;
-(void)removeOnePoint;
-(void)removeOneLife;
@end
