 //
//  Points.m
//  
//
//  Created by Ezekiel Chow on 25/11/2015.
//
//

#import "Points.h"

@implementation Points

-(id)init
{
    if ((self = [super init]))
    {
        self.points = 0;
        
        self.life =5;
    }
    
   return self;
}

-(void)addOnePoint
{
    self.points = self.points + 1;
    NSLog(@"points increase %d", self.points);
}


-(void)removeOnePoint
{
    self.points = self.points - 1;
    NSLog(@"points increase %d", self.points);
}

-(void) removeOneLife
{
    self.points = 0;
    self.life=self.life-1;

}
@end
