//
//  Prize.h
//  
//
//  Created by alex kow yong yeng on 20/11/2015.
//
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface Prize : UIViewController

- (IBAction)btnShowResult:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditResult;

- (IBAction)btnShowrResult2:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditResult2;

- (IBAction)btnShowrResult3:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditResult3;

- (IBAction)btnShowrResult4:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditResult4;

@property (strong, nonatomic) IBOutlet UILabel *prizeProgressTxt;

@property (strong, nonatomic) IBOutlet UIProgressView *prizeBar;

@property (strong, nonatomic) IBOutlet UILabel *prizeLvl;


@end
