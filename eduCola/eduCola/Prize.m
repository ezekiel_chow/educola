//
//  Prize.m
//  
//
//  Created by alex kow yong yeng on 20/11/2015.
//
//

#import "Prize.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
@interface Prize ()


@end

@implementation Prize
@synthesize btnEditResult,btnEditResult2,btnEditResult3,btnEditResult4, prizeProgressTxt, prizeBar,prizeLvl;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    static int value = 0; value++;
    prizeBar.progress += value;
    
  
    
    PFQuery *loadObject=[PFQuery queryWithClassName:@"Game"];
    [loadObject orderByDescending:@"createdAt"];
    [loadObject getFirstObjectInBackgroundWithBlock:^(PFObject *saveData, NSError *error) {
        
        prizeLvl.text = saveData[@"Level"];
        prizeProgressTxt.text = saveData[@"Milestone"];
    }];
    
    self.prizeProgressTxt.text = [NSString stringWithFormat:@"%d %%",value];
    self.prizeBar.progress = (float)value/10.0f;
    
    if(prizeBar.progress >= 0.25)
    {
        btnEditResult2.enabled=YES;
    }
    if (prizeBar.progress >= 0.55){
        btnEditResult3.enabled = YES;
    }
    if (prizeBar.progress >= 0.75){
        btnEditResult4.enabled = YES;
    }

    
   
    
    //Disable Button ~~~~~~~~~~~~~~~~~~~~~~~~
    //btnEditResult.enabled=NO;
    btnEditResult2.enabled=NO;
    btnEditResult3.enabled=NO;
    btnEditResult4.enabled=NO;

    prizeBar.progress = 0;
    prizeProgressTxt.text = 0;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnShowResult:(id)sender {
    [self increasePrizeBarValue];
}

-(void)increasePrizeBarValue{
       }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

- (IBAction)btnShowrResult2:(id)sender {
}

- (IBAction)btnShowrResult3:(id)sender {
    
}

- (IBAction)btnShowrResult4:(id)sender {
}
@end
