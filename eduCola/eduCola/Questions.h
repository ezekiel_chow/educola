//
//  Questions.h
//  
//
//  Created by Ezekiel Chow on 10/11/2015.
//
//

#import <Foundation/Foundation.h>

@interface Questions : NSObject

@property(nonatomic, retain)NSMutableDictionary *questionsDict;
@property(nonatomic, retain)NSMutableArray *animalsArray;
@property(nonatomic, assign)NSInteger *questionCount;
@property (nonatomic, assign) NSInteger correctCount;
@property (nonatomic, assign) NSInteger incorrectCount;
@property (nonatomic, readonly, strong) NSString *questionImage;
@property (nonatomic, readonly, strong) NSString *answer1;
@property (nonatomic, readonly, strong) NSString *answer2;
@property (nonatomic, readonly, strong) NSString *answer3;
@property (nonatomic, readonly, strong) NSString *answer4;
@property (nonatomic, readonly, strong) NSString *correctAnswer;
@property (nonatomic, readonly, strong) NSString *sound;
@property (nonatomic, assign) NSInteger tipCount;
@property (nonatomic, strong) NSString * tip;
@property int currentQuestion;

-initWithPlist:(NSString *)plistName;
-(void) nextQuestion;
-(void) previousQuestion;
-(void) updateSet;
-(BOOL) checkQuestion: (NSInteger) question forAnswer: (NSInteger) answer;
@end
