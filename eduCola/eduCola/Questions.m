//
//  Questions.m
//  
//
//  Created by Ezekiel Chow on 10/11/2015.
//
//

#import "Questions.h"
@interface Questions()
@property (nonatomic, strong) NSString *questionImage;
@property (nonatomic, strong) NSString *answer1;
@property (nonatomic, strong) NSString *answer2;
@property (nonatomic, strong) NSString *answer3;
@property (nonatomic, strong) NSString *answer4;
@property (nonatomic, strong) NSString *correctAnswer;
@property (nonatomic, strong) NSString *sound;
@end

@implementation Questions

-(id)initWithPlist:(NSString *)plistName
{
    if ((self = [super init]))
    {
        //chcek 4 plist
        NSFileManager *fileManger=[NSFileManager defaultManager];
        NSError *error;
        NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        
        NSString *doumentDirectoryPath=[pathsArray objectAtIndex:0];
        
        NSString *destinationPath= [doumentDirectoryPath stringByAppendingPathComponent:plistName];
        
        NSLog(@"plist path %@",destinationPath);
        if ([fileManger fileExistsAtPath:destinationPath]){
            //NSLog(@"database localtion %@",destinationPath);
            
        }else
        {
            NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:plistName];
            [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];

        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        // get documents path
        NSString *documentsPath = [paths objectAtIndex:0];
        // get the path to our Data/plist file
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:plistName];

        self.questionsDict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        self.animalsArray = [self.questionsDict objectForKey:@"Animals"];
        
    }
    
    return self;
}

- (void) previousQuestion
{
    if (self.currentQuestion <= 0)
    {
        self.currentQuestion = (int)([self.animalsArray count] - 1);
    }
    else
    {
        self.currentQuestion = self.currentQuestion - 1;
    }
    [self updateSet];
}

- (void) nextQuestion
{
    int max = (int)[self.animalsArray count] - 1;
    if (self.currentQuestion >= max)
    {
        self.currentQuestion = 0;
    }
    else
    {
        self.currentQuestion = self.currentQuestion + 1;
    }
    [self updateSet];
}

-(void)updateSet
{
    self.questionImage = self.animalsArray[self.currentQuestion][@"questionImage"];
    self.answer1 = self.animalsArray[self.currentQuestion][@"answer1"];
    self.answer2 = self.animalsArray[self.currentQuestion][@"answer2"];
    self.answer3 = self.animalsArray[self.currentQuestion][@"answer3"];
    self.answer4 = self.animalsArray[self.currentQuestion][@"answer4"];
    self.correctAnswer = self.animalsArray[self.currentQuestion][@"correct"];
    self.sound = self.animalsArray[self.currentQuestion][@"sound"];
    self.tip = self.animalsArray[self.currentQuestion][@"tip"];
    
    if (self.currentQuestion == 0) {
        self.correctCount = 0;
        self.incorrectCount = 0;
        self.tipCount = 0;
    }
}

- (BOOL) checkQuestion: (NSInteger) question forAnswer: (NSInteger) answer
{
    NSString * ans = self.animalsArray[question] [@"correct"];
    if  ([ans intValue] == answer) {
        self.correctCount++;
        return YES;
    } else  {
        self.incorrectCount++;
        return NO;
    }
}


@end
